var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser());
app.use(express.static(__dirname + '/public'));

app.listen(process.env.PORT || 4321);

app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

var data = [];

function dataConcat(arrayData, formData) {
    for (i = 0; i < formData.length; i += 1) {
        arrayData.push(formData[i]);
    }
}

function dataClean(arrayData) {
    for (i = 0; i < arrayData.lenght; i += 1) {
        delete arrayData[i];
    }
}

function countEntries(entriesArray) {
    var count = 0;
    for (i = 0; i < data.length; i += 1) {
        count += 1;
    }
    return count;
}

function raffle(entriesLenght) {
    return (Math.floor((Math.random() * entriesLenght) + 1));
}

function idFilter(input) {
    return input.substring(2, input.lenght);
}

function phoneFilter(input) {
    var ddi = input.substring(2, 5);
    var ddd = input.substring(5, 7);
    var number = input.substring(8, input.lenght);
    var output = ddi + " (" + ddd + ") " + number;
    return (output);
}

function drawn() {
    var id = raffle(countEntries(data));
    return {
        id: idFilter(data[id]['id']),
        name: data[id]['full_name'],
        city: data[id]['city'],
        contact: {
            email: data[id]['email'].toLowerCase(),
            phone: phoneFilter(data[id]['phone_number'])
        }
    };
}

app.get('/api/sorteado', function(req, res) {
    res.send(drawn());
});

app.get('/api/quantidade', function(req, res) {
    var objSend = { date: Date(), qttEntries: countEntries(data) };
    res.send(objSend);
});

app.get('/api/records', function(req, res) {
    res.send(data);
});

app.get('/api/records/:id', function(req, res) {
    var reqId = req.params.id;
    var objData = {};
    for (i = 0; i < data.length; i += 1) {
        if (data[i].id === reqId) {
            objData = data[i];
        }
    }
    res.send(objData).status(200);
});

app.delete('/api/records/del/:id', function(req, res) {
    var reqId = req.params.id;
    var objData = "";
    for (i = 0; i < data.length; i += 1) {
        if (reqId === data[i].id) {
            objData = data[i];
            delete data[i];
        }
    }
    if (objData !== "") {
        res.send(true).status(200);
    } else {
        res.send(false);
    }
});

app.delete('/api/records/clean', function(req, res) {
    dataClean(data);
    res.send(true).status(200);
})

app.post('/api/upload', function(req, res) {
    var obj = req.body;
    console.log(obj);
    dataConcat(data, obj);
    res.send(true).status(200);
});
